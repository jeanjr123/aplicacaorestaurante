import { Component, OnInit } from '@angular/core';

import{Restaurant} from './restaurant/restaurant.model'
import { RestaurantsService } from './restaurants.service';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html'
 
})
export class RestaurantsComponent implements OnInit {

  //array de restaurantes onde o input da pagina restaurant componente ts  vai buscar

  restaurants: Restaurant[] 
   //declarando o servico no construtor e recebendo a propriedade
  constructor(private restaurantsService: RestaurantsService) { }

  ngOnInit() {
    //faco o subscribe aqui na pagina de componente para fazer a requisicao na api
      this.restaurantsService.restaurants()
         // no subscribe estou pegando o que recebi e estou passando para propriedade
        .subscribe(restaurants => this.restaurants = restaurants)
      
  }

}
