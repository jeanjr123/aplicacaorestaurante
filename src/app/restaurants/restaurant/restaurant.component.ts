import { Component, OnInit, Input} from '@angular/core';

//importando o tipo de representacao de dados da pagina restaurant model ts
import {Restaurant} from './restaurant.model'

@Component({
  selector: 'mt-restaurant',
  templateUrl: './restaurant.component.html'
 
})
export class RestaurantComponent implements OnInit {

  //o input restaurant esta com o tipo `Restaurant` definido na representacao de dados
  // da pagina restaurant model ts

  @Input() restaurant: Restaurant

  constructor() { }

  ngOnInit() {
  }

}
