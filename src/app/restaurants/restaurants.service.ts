import {Injectable} from '@angular/core'

//importando servico http
import {Http} from '@angular/http'

import { Restaurant } from "./restaurant/restaurant.model";

import {MEAT_API} from "../app.api"

import {Observable} from 'rxjs/Observable'
//importando o operador
import 'rxjs/add/operator/map'

//importando o metodo cath

import 'rxjs/add/operator/catch'


import {ErrorHandler} from '../app.error-handler'


import {MenuItem} from '../restaurant-detail/menu-item/menu-item.model'
//criando classe de servico e injetando no componente

@Injectable()
export class RestaurantsService {


    //recebendo a injecao de dados do servico http
    constructor(private http : Http){ }




    //restaurants() e' um metodo que retorna um arrray de restaurantes

    // estou retornando observale onde o tipo da resposta vai vir um array de restaurantes

    restaurants(): Observable<Restaurant[]> {
         return this.http.get(`${MEAT_API}/restaurants`)
         //substitui a resposta pelo json apenas
         // a funcao map mapeia e transforma a resposta responsive em um array de restaurantes
         .map(response => response.json())
         //cath eu trato um erro trocando por outra observable ou simplesmente disparar um erro
         .catch(ErrorHandler.handleError)
    }  

     //Retornando um restaurante de cada vez da api por id, o tipo do parametro da funcao e string
     restaurantById(id:string): Observable<Restaurant>{
          //quando coloco id desse jeito a api tem que retornar so o id
           return this.http.get(`${MEAT_API}/restaurants/${id}`)
           .map(response => response.json())
           .catch(ErrorHandler.handleError)
     }
     //criando um metodo chamado review que retorna um observable any
     reviewsOfRestaurant(id: string): Observable<any>{
            return this.http.get(`${MEAT_API}/restaurants/${id}/reviews`)
            .map(response => response.json())
            .catch(ErrorHandler.handleError)
     }

     menuOfRestaurnt(id: string): Observable<MenuItem[]>{
         return this.http.get(`${MEAT_API}/restaurants/${id}/menu`)
         .map(response => response.json())
         .catch(ErrorHandler.handleError)

     }

    

}