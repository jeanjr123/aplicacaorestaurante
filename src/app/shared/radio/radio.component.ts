//inporto input porque as opcoes do array radioOption vem de fora

import { Component, OnInit, Input, forwardRef } from '@angular/core';

import {NG_VALUE_ACCESSOR, ControlValueAccessor} from '@angular/forms';

import { RadioOption } from './radio-option.model';

@Component({
  selector: 'mt-radio',
  templateUrl: './radio.component.html',
  providers: [
   {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => RadioComponent),
    multi: true,
   }

  ]
})
export class RadioComponent implements OnInit, ControlValueAccessor {

  @Input() options: RadioOption[]

  value: any

  onChange: any

  constructor() { }

  ngOnInit() {
  }

  setValue(value: any){
     this.value = value
     //chama onChange aqui para mudar de valor
     this.onChange(this.value)
  }


   /**
     * Write a new value to the element.
     * esse metodo é so para passar um valor para o componente, valor recebe obj passado por parametro
     */
    writeValue(obj: any): void {
       this.value = obj
    }
    /**
     * Set the function to be called when the control receives a change event.
     */
    registerOnChange(fn: any): void{
      this.onChange = fn
    }
        
    /**
     * Set the function to be called when the control receives a touch event.
     * metodo usado para avisar que entrou no componente
     */
    registerOnTouched(fn: any): void{
       
    }
    /**
     * This function is called when the control status changes to or from "DISABLED".
     * Depending on the value, it will enable or disable the appropriate DOM element.
     *
     * @param isDisabled
     */
    setDisabledState?(isDisabled: boolean): void {};

}
