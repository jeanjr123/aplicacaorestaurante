//item do carrinho de compras tem a quantidade a mais que o carrinho do menu

import {MenuItem} from '../menu-item/menu-item.model'


//a classe menu item tem tudo que a classe item tem + a quantidade
export class CartItem {                        
     constructor (public menuItem: MenuItem,
                  public quantity: number = 1){}//default 1


//metodo value

value() : number {
    return this.menuItem.price * this.quantity
}


}


