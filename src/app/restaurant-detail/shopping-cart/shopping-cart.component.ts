import { Component, OnInit } from '@angular/core';


//para acessar os dados e metodos do cart service no html, eu preciso importar a classe
//ShoppingCartService que foi exportada na pagina shopping-cart.service.ts
//depois de importado vai ser injetado no ngOnInit


import {ShoppingCartService} from './shopping-cart.service'


@Component({
  selector: 'mt-shopping-cart',
  templateUrl: './shopping-cart.component.html'
})
export class ShoppingCartComponent implements OnInit {

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
  }
  //metodo para expor os itens

  items(): any[] {
    //busca os itens da classe ShoppingCartService da pagina service
     return this.shoppingCartService.items;

  }

  clear(){
    this.shoppingCartService.clear()
  }

  removeItem(item:any){
    this.shoppingCartService.removeItem(item)
  }

  addItem(item: any){
    this.shoppingCartService.addItem(item)
  }

  total(): number {
     return this.shoppingCartService.total()
  }

}
