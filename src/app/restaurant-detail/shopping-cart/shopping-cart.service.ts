import {CartItem} from './cart-item.model';
import {MenuItem} from '../menu-item/menu-item.model'



export class ShoppingCartService{
    items:CartItem[] = []
    
    //metodo clear usado para limpar dados do carrinho
    clear(){
       this.items = []

    }

    //metodo do botao para adicionar itens no carrinho
    addItem(item:MenuItem){
                        // no add item recebo um item como parametro
                        // a funcao abaixo verifica se o id do item que recebi como parametro
                        // e igual ao id do menu item
       let foundItem = this.items.find((mItem)=>mItem.menuItem.id === item.id)
       // se eu encontrar eu adiciono uma quantidade ao item
       if(foundItem){
           this.increaseQty(foundItem)
       }else{
          //se eu nao encontrar eu so passo o item do menu,com a quantidade default 1
          this.items.push(new CartItem(item))

       }


    }

    increaseQty(item: CartItem){
       item.quantity = item.quantity + 1 
    }

    decreaseQty(item: CartItem){
       item.quantity = item.quantity - 1
       if(item.quantity === 0){
           this.removeItem(item)          
       }
    }


     //remover item
    removeItem(item:CartItem){
        this.items.splice(this.items.indexOf(item), 1);      
    }

    //metodo para soma dos itens do carrinho, tipo number
    total(): number{
       return this.items
       // primeiro o map troca o item pelo valor do item, troca o array de itens pelos valores 
       //dos itens
       .map(item => item.value())
       // reduce tem o valor anterior e o atual e soma os dois
       //obs: o valor inicial é igual a zero 
       .reduce((prev,value)=> prev+value,0)

    }


}