import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {MenuItem} from './menu-item.model';

@Component({
  selector: 'mt-menu-item',
  templateUrl: './menu-item.component.html'
})
export class MenuItemComponent implements OnInit {

  //criando uma propriedade que represente um item do menu, faco isso criando um model e 
  //exportando uma interface
  //importo em cima
  //adiciono o imput 
  @Input() menuItem : MenuItem
  
  @Output() add = new EventEmitter()


  constructor() { }

  ngOnInit() {
  }

  emitAddEvent(){
    //quando eu emito um evento dizendo que o menu item foi clicado,
    // o meu componente parent associa uma acao e faz algo faz a busca
    // obs: o parent fica no restaurant service
     this.add.emit(this.menuItem);

  }

}
