//Para usar o routes eu tenho que importa-lo

import {Routes} from '@angular/router'

//para usar o home component precisamos importa-lo
import { HomeComponent } from './home/home.component';
import { RestaurantsComponent } from './restaurants/restaurants.component';
import { RestaurantDetailComponent } from './restaurant-detail/restaurant-detail.component';
import { AboutComponent } from './about/about.component';
import { MenuComponent } from './restaurant-detail/menu/menu.component';
import { ReviewsComponent } from './restaurant-detail/reviews/reviews.component';
import {OrderComponent} from './order/order.component'
import { OrderSummaryComponent } from './order-summary/order-summary.component';







//dentro do array eu tenho os caminhos de cada rota

export const ROUTES: Routes = [
    //se nao colocar caminho na path ele mostra o componente principal\

    {path: '', component: HomeComponent},
 

    {path:'restaurants', component: RestaurantsComponent},
    //children serve para fazer uma rota filha
    {path:'restaurants/:id' , component: RestaurantDetailComponent,
       children: [
          {path: '' , redirectTo: 'menu', pathMatch: 'full'},
  
          {path: 'menu', component: MenuComponent },

          {path: 'reviews', component: ReviewsComponent}



       ] },
       
    {path: 'order', component: OrderComponent},

    {path: 'order-summary' , component: OrderSummaryComponent},

    //quando encontrar a url about, precisa mostrar o component about
    {path: 'about' , component: AboutComponent}



]